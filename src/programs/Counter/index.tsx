import { increment, decrement, incrementByAmount } from 'redux/features/counter/slice';
import { useAppDispatch, useAppSelector } from 'core/hooks/useStore';
import { useState } from 'react';

function Counter() {
  const [amount, setAmount] = useState(0);
  const count = useAppSelector((state) => state.counter.value);
  const dispatch = useAppDispatch();

  return (
    <>
      count is {count}
      <br />
      <button onClick={() => dispatch(increment())}>+</button>
      <button onClick={() => dispatch(decrement())}>-</button>
      <br />
      <br />
      Increment by :
      <input type='number' value={amount} onChange={(e) => setAmount(Number(e.target.value))} />
      <button onClick={() => dispatch(incrementByAmount(amount))}>++</button>
    </>
  );
}

export default Counter;
