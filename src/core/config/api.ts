const apiUrl = import.meta.env.VITE_API_URL;
const apiPrefix = "/api";

const baseUrl = apiUrl + apiPrefix;

export const getUrlParams = () => ({apiUrl, apiPrefix, baseUrl})