import Counter from 'programs/Counter'

function App() {
 
  return (
    <>
      <Counter />
    </>
  )
}

export default App
